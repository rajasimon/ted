from django.db import models

# Create your models here.
class Tag(models.Model):
    name = models.CharField(max_length=220, null=True, blank=True)

    def __str__(self):
        return "{}".format(self.name)


class Rating(models.Model):
    rating_id = models.IntegerField(null=True, blank=True)
    name = models.CharField(max_length=220, null=True, blank=True)
    count = models.IntegerField(null=True, blank=True)

    def __str__(self):
        return "{}".format(self.name)


class Ted(models.Model):
    description = models.TextField(null=True, blank=True)
    event = models.CharField(max_length=220, null=True, blank=True)
    main_speaker = models.CharField(max_length=220, null=True, blank=True)
    name = models.CharField(max_length=200, null=True, blank=True)
    published_date = models.DateTimeField(null=True, blank=True)
    ratings = models.ManyToManyField(Rating, blank=True)
    related_talks = models.ManyToManyField('self', blank=True)
    speaker_occupation = models.CharField(max_length=220, null=True, blank=True)
    tags = models.ManyToManyField(Tag, blank=True)
    title = models.CharField(max_length=200, null=True, blank=True)
    url = models.URLField(null=True, blank=True)
    views = models.IntegerField(null=True, blank=True)

    def __str__(self):
        return "{}".format(self.name)
        