from django.urls import path

from tedtalk.api import views

urlpatterns = [
    path('api/tags/', views.tag_list),
    path('api/teds/', views.ted_list)
]
