import operator
from functools import reduce

from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.db.models import Q

from .models import Ted, Tag
from .serializers import TagSerializer, TedSerializer


# Create your views here.
@csrf_exempt 
def tag_list(request):
    tags = Tag.objects.all()
    serializer = TagSerializer(tags, many=True)
    return JsonResponse(serializer.data, safe=False)


@csrf_exempt
def ted_list(request):
    # If tags present in the request return the relevant information
    tags = request.GET.get('tags', None)
    if tags:
        clauses = (Q(tags__name__contains=x) for x in tags.split(','))
        query = reduce(operator.or_, clauses)
        teds = Ted.objects.filter(query)
        print(teds.count())
    else:
        teds = Ted.objects.all()
    serializer = TedSerializer(teds, many=True)
    return JsonResponse(serializer.data, safe=False)
