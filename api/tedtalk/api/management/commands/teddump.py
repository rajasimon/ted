import csv
import ast
import argparse
from datetime import datetime

from django.core.management.base import BaseCommand, CommandError
from tedtalk.api.models import Tag, Rating, Ted


class Command(BaseCommand):
    help = 'Dump sample csv to sqlite'

    def add_arguments(self, parser):
        parser.add_argument(
            '--file', 
            dest='file', 
            required=True,
            type=argparse.FileType('r', encoding="iso8859-1"),
            help='Dump file',
        )

    def handle(self, *args, **options):
        csv_file = options['file']
        
        reader = csv.DictReader(csv_file)

        count = 0
        for row in reader:
            description = row['description']
            event = row['event']
            main_speaker = row['main_speaker']
            name = row['name']
            published_date = row['published_date']
            ratings = row['ratings']
            related_talks = row['related_talks']
            speaker_occupation = row['speaker_occupation']
            tags = row['tags']
            title = row['title']
            url = row['url']
            views = row['views']

            # Main ted instance creation
            ted, created = Ted.objects.get_or_create(url=url)
            
            ted.description = description
            ted.event = event
            ted.main_speaker = main_speaker
            ted.name = name
            ted.published_date = datetime.fromtimestamp(int(published_date))
            
            # For ratings 
            ratings = ast.literal_eval(ratings)
            for rating in ratings:
                rating_obj, created = Rating.objects.get_or_create(rating_id=rating['id'])

                # If created or not created doesn't matter
                rating_obj.name = rating['name']
                rating_obj.count = rating['count']
                rating_obj.save()
            
                ted.ratings.add(rating_obj)

            # For related_talks
            related_talks = ast.literal_eval(related_talks)
            for related_talk in related_talks:
                related_talk_obj = Ted.objects.filter(url__contains=related_talk['slug']).first()
                
                if not related_talk_obj:
                    related_talk_obj, created = Ted.objects.get_or_create(url='https://www.ted.com/talks/{}'.format(related_talk['slug']))
                
                ted.related_talks.add(related_talk_obj)

            ted.speaker_occupation = speaker_occupation

            # for tags
            tags = ast.literal_eval(tags)
            for tag in tags:
                tag, created = Tag.objects.get_or_create(name=tag)
                ted.tags.add(tag)

            
            ted.title = title
            ted.views = views
            
            # Finally make sure everyting added
            ted.save()
            count += 1

        print("Total {}".format(count))