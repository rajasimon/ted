from django.contrib import admin

# Register your models here.
from .models import Tag, Rating, Ted

admin.site.register(Tag)
admin.site.register(Rating)
admin.site.register(Ted)
