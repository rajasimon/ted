from rest_framework import serializers
from .models import Ted, Tag


class TagSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tag
        fields = '__all__'


class TedSerializer(serializers.ModelSerializer):
    class Meta:
        model = Ted
        fields = '__all__'
