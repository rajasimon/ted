SCALING

Scaling database is not easy. And to serve upto 5 million users we need to come
up with better solutions like database cache. So that every time user requested
something we can get it from the cache and serve it. To use this in frontend we need 
common api to support this architecture. Like django supports memcache for particual route. 

And also we need transaction table for some use cases like ratings for the particular ted talk. 
So may be we listen database insert or update operation and we calucalte ratings and update. 

Also we can move some common frequently requested items like title and other informations
to NoSQL databases. NoSQL is very fast it serves from RAM. And we can use this for 
frontend design. 

