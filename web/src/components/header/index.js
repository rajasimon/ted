import { h } from 'preact';
import { Link } from 'preact-router/match';
import style from './style';

const Header = () => (
	<div>
		<nav class="navbar has-shadow is-spaced" role="navigation" aria-label="main navigation">
			<div class="navbar-brand">
				<a class="navbar-item" href="/">
				<h1 class="title">TED</h1>
				</a>

				<a role="button" class="navbar-burger" aria-label="menu" aria-expanded="false">
				<span aria-hidden="true"></span>
				<span aria-hidden="true"></span>
				<span aria-hidden="true"></span>
				</a>
			</div>
			<div class="navbar-end">
				<div class={style.navbarwidth + " navbar-item"}>
					<div class={"navbarfieldwidth " + style.navbarfieldwidth}>
					<div class="control">
						<input class="input is-medium is-fullwidth" type="text" placeholder="Search..." />
					</div>
					</div>
				</div>
			</div>
		</nav>
	</div>
);

export default Header;
