import { h, Component } from 'preact';
import { Link } from 'preact-router/match';
import { route } from 'preact-router';
import style from './style';

export default class Home extends Component {

	async componentDidMount() {
		this.setState({tagList: []})

		let res = await fetch('http://localhost:8000/api/tags/'),
		json = await res.json(),
		results = json && json || [];
		this.setState({ results });
	}

	selectTag = e => {
		e.target.classList.add("is-success")
		
		// Get the selected tag name
		if (this.state.tagList.indexOf(e.target.innerText) == -1) {
			this.setState({
				tagList: [...this.state.tagList, e.target.innerText]
			})
		} else {
			const tempArray = this.state.tagList
			var index = tempArray.indexOf(e.target.innerText)
			tempArray.splice(index, 1);
			this.setState({tagList: tempArray});

			e.target.classList.remove("is-success")
			e.target.classList.remove("is-danger")
		}
	}

	tagMouseOver = e => {
		// Get the selected tag name
		if (this.state.tagList.indexOf(e.target.innerText) != -1) {
			e.target.classList.add('is-danger')
			e.target.classList.remove("is-success")
		}
	}

	tagMouseOut = e => {
		// Get the selected tag name
		if (this.state.tagList.indexOf(e.target.innerText) != -1) {
			e.target.classList.add("is-success")
			e.target.classList.remove('is-danger')
		}
	}

	applyTagFilter = e => {
		fetch('http://localhost:8000/api/teds/?tags=' + this.state.tagList)
		.then((resp) => resp.json())
		.then((teds) => this.setState( { teds }))
	}

	render({}, { results=[], tagList=[], teds=[] }) {
		console.log(teds)
		if ( teds.length != 0 ) {
			return (
				<div class={style.home}>
					<div class="section">
						<div class="container">
							<div class="columns is-multiline">
								{ teds.map( result => (
									<Result result={result} />
								)) }
							</div>
						</div>
					</div>
				</div>
			)
		} else {

			return (
				<div class={style.home}>
					<div class="section">
						<div class="container">
							<div class="notification">
								Select tag and click continue
							</div>
							<div class="tags">
								{ results.map( result => (
									<span 
										class="tag is-medium" 
										onClick={this.selectTag}
										onMouseOver={this.tagMouseOver}
										onMouseOut={this.tagMouseOut}>
										
										{ result.name}
									</span>
									)) }
							</div>
							<button class="button is-primary" onClick={this.applyTagFilter}>Submit</button>
						</div>
					</div>
				</div>
			)
		}
	}
}

const Result = ({ result }) => (
	<div class="column is-one-third">
		<div class="card is-fullwidth">
		<div class="card-content">
			<p class="title">
			{result.title}
			</p>
			<p class="subtitle">
			{result.main_speaker}
			</p>
		</div>
		<footer class="card-footer">
			<p class="card-footer-item">
			<span>
				<a href={result.url} target="_blank">Watch</a>
			</span>
			</p>
		</footer>
		</div>
	</div>
);